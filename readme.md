# Laravel Turbolinks

Helpers and traits for using Turbolinks in Laravel.

## Installation

`composer require slyfoxcreative/laravel-turbolinks`

## Getting Started

1. Add middleware classes in `src/Middleware` to web middleware group
2. Add `\SlyFoxCreative\Turbolinks\Exceptions\HandlesInvalidJson` trait to `App\Exceptions\Handler`
3. Add `\SlyFoxCreative\Turbolinks\Auth\AuthenticatesUsers` trait to `App\Http\Controllers\Auth\LoginController`
4. Add `\SlyFoxCreative\Turbolinks\Auth\RegistersUsers` trait to `App\Http\Controllers\Auth\RegisterController`
5. Run `artisan vendor:publish --tag=resources`
