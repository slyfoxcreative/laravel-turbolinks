<?php

declare(strict_types=1);

namespace SlyFoxCreative\Turbolinks;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Laravel\Dusk\Browser;
use SlyFoxCreative\Turbolinks\Routing\Redirector;

class ServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        $this->app->singleton('redirect', function ($app) {
            $redirector = new Redirector($app['url']);

            // If the session is set on the application instance, we'll inject
            // it into the redirector instance. This allows the redirect
            // responses to allow for the quite convenient "with" methods that
            // flash to the session.
            if (isset($app['session.store'])) {
                $redirector->setSession($app['session.store']);
            }

            return $redirector;
        });
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../js/remote_controller.js' => resource_path('js/controllers/remote_controller.js'),
        ], 'resources');

        /* Pause before waiting for a location to prevent Dusk from getting
         * confused by a Turbolinks navigation.
         */
        if (!$this->app->isProduction()) {
            Browser::macro('pauseForLocation', function ($location, $time = 300) {
                return $this->pause($time)->waitForLocation($location);
            });
        }
    }
}
