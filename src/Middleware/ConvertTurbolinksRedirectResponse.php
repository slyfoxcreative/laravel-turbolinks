<?php

declare(strict_types=1);

namespace SlyFoxCreative\Turbolinks\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;

class ConvertTurbolinksRedirectResponse
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (is_a($response, RedirectResponse::class) && $request->expectsJson()) {
            return response()->json([
                'location' => $response->headers->get('Location'),
            ]);
        }

        return $response;
    }
}
