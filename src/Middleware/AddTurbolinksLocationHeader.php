<?php

declare(strict_types=1);

namespace SlyFoxCreative\Turbolinks\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;

class AddTurbolinksLocationHeader
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (session()->has('_turbolinks_location') && !is_a($response, RedirectResponse::class)) {
            $response->header('Turbolinks-Location', session()->pull('_turbolinks_location'));
        }

        return $response;
    }
}
