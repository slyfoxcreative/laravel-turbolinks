<?php

declare(strict_types=1);

namespace SlyFoxCreative\Turbolinks\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;

trait HandlesInvalidJson
{
    /**
     * Convert a validation exception into a JSON response.
     */
    protected function invalidJson($request, ValidationException $exception)
    {
        return $this->invalid($request, $exception);
    }
}
