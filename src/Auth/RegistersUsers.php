<?php

declare(strict_types=1);

namespace SlyFoxCreative\Turbolinks\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers as OriginalRegistersUsers;
use Illuminate\Http\Request;

trait RegistersUsers
{
    use OriginalRegistersUsers;

    /**
     * The user has been registered.
     */
    protected function registered(Request $request, $user)
    {
        return redirect($this->redirectPath());
    }
}
