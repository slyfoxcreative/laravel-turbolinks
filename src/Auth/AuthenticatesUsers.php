<?php

declare(strict_types=1);

namespace SlyFoxCreative\Turbolinks\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers as OriginalAuthenticatesUsers;
use Illuminate\Http\Request;

trait AuthenticatesUsers
{
    use OriginalAuthenticatesUsers;

    /**
     * The user has been authenticated.
     */
    protected function authenticated(Request $request, $user)
    {
        return redirect()->intended($this->redirectPath());
    }

    /**
     * The user has logged out of the application.
     */
    protected function loggedOut(Request $request)
    {
        return redirect('/');
    }
}
