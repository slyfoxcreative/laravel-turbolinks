<?php

declare(strict_types=1);

namespace SlyFoxCreative\Turbolinks\Routing;

use Illuminate\Routing\Redirector as BaseRedirector;

class Redirector extends BaseRedirector
{
    /**
     * Create a new redirect response.
     */
    protected function createRedirect($path, $status, $headers)
    {
        session()->put('_turbolinks_location', $path);

        return parent::createRedirect($path, $status, $headers);
    }
}
