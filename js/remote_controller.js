/* global Turbolinks */

import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ["button", "form"]

  submit(event) {
    event.preventDefault()
    if (this.data.get("disabled") === "1") {
      return
    }
    this.disable()

    if (document.querySelector("body").dataset.environment === "testing") {
      // Submit the form normally in Dusk tests
      this.formTarget.submit()
    } else if (this.formTarget.method.toLowerCase() === "get") {
      this.get()
    } else {
      this.post()
    }
  }

  disable() {
    this.data.set("disabled", "1")
    this.buttonTarget.classList.add("disabled")
  }

  enable() {
    this.data.set("disabled", "0")
    this.buttonTarget.classList.remove("disabled")
  }

  get() {
    const queryString = new URLSearchParams(
      new FormData(this.formTarget),
    ).toString()
    Turbolinks.visit(`${this.formTarget.action}?${queryString}`)
  }

  post() {
    const headers = new Headers()
    headers.append("Accept", "application/json")

    fetch(this.formTarget.action, {
      method: "POST",
      headers: headers,
      body: this.formData(),
    })
      .then((response) => {
        if (response.status === 413) {
          this.alert("Your uploaded file is too large.")
          return
        }

        if (response.status === 403 || response.status === 500) {
          response.json().then((data) => {
            this.alert(data.message)
          })
          return
        }

        if (response.ok) {
          Turbolinks.clearCache()
        }

        response.json().then((data) => {
          if (data.location) {
            Turbolinks.visit(data.location)
          } else if (data.html && data.selector) {
            const element = document.querySelector(data.selector)
            element.innerHTML = data.html
            element.scrollIntoView()
          } else {
            console.log(data)
            this.alert("Invalid response.")
          }
        })
      })
      .finally(() => {
        this.enable()
      })
  }

  alert(message) {
    const alert = document.createElement("div")
    alert.classList.add("alert", "alert-danger")
    alert.innerHTML = message

    const main = document.querySelector("main")

    main.prepend(alert)
  }

  formData() {
    const elements = Array.from(this.formTarget.elements).filter((element) => {
      return (
        (element.type !== "checkbox" && element.type !== "radio") ||
        element.checked
      )
    })

    return elements.reduce((data, element) => {
      data.append(
        element.name,
        element.type === "file" && element.files[0]
          ? element.files[0]
          : element.value,
      )

      return data
    }, new FormData())
  }
}
